package services

import (
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/config"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/product_service"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Catalog Service
	ProductService() product_service.ProductServiceClient
	CategoryService() product_service.CategoryServiceClient

	// User Service
	BranchService() user_service.BranchServiceClient
	UserService() user_service.UserServiceClient
	CourierService() user_service.CourierServiceClient
	ClientService() user_service.ClientServiceClient

	// Order Service
	OrderService() order_service.OrderServiceClient
	OrderProductService() order_service.OrderProductServiceClient
	DeliveryTarifService() order_service.DeliveryTarifServiceClient
	DeliveryTarifValueService() order_service.DeliveryTarifValueServiceClient
}

type grpcClients struct {
	// Catalog Service
	productService  product_service.ProductServiceClient
	categoryService product_service.CategoryServiceClient

	// User Service
	branchService  user_service.BranchServiceClient
	userService    user_service.UserServiceClient
	courierService user_service.CourierServiceClient
	clientService  user_service.ClientServiceClient

	// Order Service
	orderService              order_service.OrderServiceClient
	orderProductService       order_service.OrderProductServiceClient
	deliveryTarifService      order_service.DeliveryTarifServiceClient
	deliveryTarifValueService order_service.DeliveryTarifValueServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Catalog Microservice
	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// User Microservice
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Order Microservice
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Product Service
		productService:  product_service.NewProductServiceClient(connCatalogService),
		categoryService: product_service.NewCategoryServiceClient(connCatalogService),
		// User Service
		branchService:  user_service.NewBranchServiceClient(connUserService),
		userService:    user_service.NewUserServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),
		clientService:  user_service.NewClientServiceClient(connUserService),

		// Order Service
		orderService:              order_service.NewOrderServiceClient(connOrderService),
		orderProductService:       order_service.NewOrderProductServiceClient(connOrderService),
		deliveryTarifService:      order_service.NewDeliveryTarifServiceClient(connOrderService),
		deliveryTarifValueService: order_service.NewDeliveryTarifValueServiceClient(connOrderService),
	}, nil
}

// Catalog Service
func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

// User Service
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

// Order Service
func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) OrderProductService() order_service.OrderProductServiceClient {
	return g.orderProductService
}

func (g *grpcClients) DeliveryTarifService() order_service.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}

func (g *grpcClients) DeliveryTarifValueService() order_service.DeliveryTarifValueServiceClient {
	return g.deliveryTarifValueService
}
