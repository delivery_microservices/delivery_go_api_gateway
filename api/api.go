package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/delivery_microservices/delivery_go_api_gateway/api/docs"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/api/handler"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/config"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))

	v1 := r.Group("/v1")

	v1.POST("/auth/sign-in", h.SignIn)
	v1.POST("/change-password", h.ChangePassword)

	product := v1.Group("/products")
	{
		product.POST("/", h.CreateProduct)
		product.GET("/", h.GetListProduct)
		product.GET("/:id", h.GetProduct)
		product.PUT("/:id", h.UpdateProduct)
		product.DELETE("/:id", h.DeleteProduct)
	}

	category := v1.Group("/categories")
	{
		category.POST("/", h.CreateCategory)
		category.GET("/", h.GetListCategory)
		category.GET("/:id", h.GetCategory)
		category.PUT("/:id", h.UpdateCategory)
		category.DELETE("/:id", h.DeleteCategory)
	}

	user := v1.Group("/users")
	{
		user.POST("/", h.CreateUser)
		user.GET("/", h.GetListUser)
		user.GET("/:id", h.GetUser)
		user.PUT("/:id", h.UpdateUser)
		user.DELETE("/:id", h.DeleteUser)
	}

	client := v1.Group("/clients")
	{
		client.POST("/", h.CreateClient)
		client.GET("/", h.GetListClient)
		client.GET("/:id", h.GetClient)
		client.PUT("/:id", h.UpdateClient)
		client.DELETE("/:id", h.DeleteClient)
	}

	courier := v1.Group("/couriers")
	{
		courier.POST("/", h.CreateCourier)
		courier.GET("/", h.GetListCourier)
		courier.GET("/:id", h.GetCourier)
		courier.PUT("/:id", h.UpdateCourier)
		courier.DELETE("/:id", h.DeleteCourier)
	}

	branch := v1.Group("/branches")
	{
		branch.POST("/", h.CreateBranch)
		branch.GET("/", h.GetListBranch)
		branch.GET("/:id", h.GetBranch)
		branch.PUT("/:id", h.UpdateBranch)
		branch.DELETE("/:id", h.DeleteBranch)
		branch.GET("/active", h.GetListActiveBranch)
	}

	order := v1.Group("/orders")
	{
		order.POST("/", h.CreateOrder)
		order.GET("/", h.GetListOrder)
		order.GET("/:id", h.GetOrder)
		order.PUT("/:id", h.UpdateOrder)
		order.DELETE("/:id", h.DeleteOrder)
		order.PUT("/change-status/:id", h.ChangeOrderStatus)
	}

	order_product := v1.Group("/order_products")
	{
		order_product.POST("/", h.CreateOrderProduct)
		order_product.GET("/", h.GetListOrderProduct)
		order_product.GET("/:id", h.GetOrderProduct)
		order_product.PUT("/:id", h.UpdateOrderProduct)
		order_product.DELETE("/:id", h.DeleteOrderProduct)
	}

	delivery_tarif := v1.Group("/delivery_tarif")
	{
		delivery_tarif.POST("/", h.CreateDeliveryTarif)
		delivery_tarif.GET("/", h.GetListTarif)
		delivery_tarif.GET("/:id", h.GetDeliveryTarif)
		delivery_tarif.PUT("/:id", h.UpdateTarif)
		delivery_tarif.DELETE("/:id", h.DeleteTarif)
	}

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
