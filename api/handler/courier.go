package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/user_service"
)

// CreateCourier godoc
// @Router       /v1/couriers [post]
// @Summary      Create a new courier
// @Description  Create a new courier with the provided details
// @Tags         Couriers
// @Accept       json
// @Produce      json
// @Param        Couriers     body  user_service.CreateCourierReq  true  "data of the user"
// @Success      201  {object}  user_service.CreateCourierResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateCourier(ctx *gin.Context) {
	var courier = user_service.Courier{}

	err := ctx.ShouldBindJSON(&courier)
	if err != nil {
		h.handlerResponse(ctx, "CreateCourier", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Create(ctx, &user_service.CourierCreateReq{
		FirstName:     courier.FirstName,
		LastName:      courier.LastName,
		Active:        courier.Active,
		Login:         courier.Login,
		Password:      courier.Password,
		MaxOrderCount: courier.MaxOrderCount,
		BranchId:      courier.BranchId,
	})

	if err != nil {
		h.handlerResponse(ctx, "CourierService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create courier response", http.StatusOK, resp)
}

// ListCouriers godoc
// @Router       /v1/couriers [get]
// @Summary      List couriers
// @Description  Get couriers
// @Tags         Couriers
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   user_service.Courier
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListCourier(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("offset", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().GetList(ctx.Request.Context(), &user_service.CourierGetListReq{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListCourier", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list courier response", http.StatusOK, resp)
}

// GetCourier godoc
// @Router       /v1/couriers/{id} [get]
// @Summary      Get a courier by ID
// @Description  Retrieve a courier by its unique identifier
// @Tags         Couriers
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "User ID to retrieve"
// @Success      200  {object}  user_service.Courier
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetCourier(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.CourierService().GetById(ctx.Request.Context(), &user_service.CourierIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error courier GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get courier response", http.StatusOK, resp)
}

// UpdateCourier godoc
// @Router       /v1/couriers/{id} [put]
// @Summary      Update an existing courier
// @Description  Update an existing courier with the provided details
// @Tags         Couriers
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "User ID to update"
// @Param        courier   body    user_service.CourierUpdateReq  true    "Updated data for the courier"
// @Success      200  {object}  user_service.CourierUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateCourier(ctx *gin.Context) {
	var courier = user_service.Courier{}
	courier.Id = ctx.Param("id")
	err := ctx.ShouldBindJSON(&courier)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Update(ctx.Request.Context(), &user_service.CourierUpdateReq{
		Id:        courier.Id,
		FirstName: courier.FirstName,
		LastName:  courier.LastName,
		Active:    courier.Active,
		Login:     courier.Login,
		Password:  courier.Password,
	})

	if err != nil {
		h.handlerResponse(ctx, "error courier Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update courier response", http.StatusOK, resp.Msg)
}

// DeleteCourier godoc
// @Router       /v1/couriers/{id} [delete]
// @Summary      Delete a courier
// @Description  Delete a courier by its unique identifier
// @Tags         Couriers
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "User ID to retrieve"
// @Success      200  {object}  user_service.CourierDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteCourier(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.CourierService().Delete(ctx.Request.Context(), &user_service.CourierIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error courier Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete courier response", http.StatusOK, resp.Msg)
}
