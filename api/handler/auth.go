package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/config"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/user_service"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/pkg/helper"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/pkg/logger"
)

type SignInReq struct {
	Username string `json:"login"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type SignInResp struct {
	Token string `json:"token"`
}

// 2. Auth: login(activeligini tekshirish kerak),changePassvord -> SMTP protocol

// @Router       /v1/auth/sign-in [post]
// @Summary      sign in staff
// @Description  api for sign in staff
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        staff    body     SignInReq  true  "data of staff"
// @Success      200  {object}  SignInResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) SignIn(ctx *gin.Context) {
	var req SignInReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	if req.Role == "user" {
		user, err := h.services.UserService().GetByUsername(ctx, &user_service.UserGetByUsernameReq{
			Username: req.Username,
		})
		if err != nil {
			h.log.Error("error while getting user by username:", logger.Error(err))
			h.handlerResponse(ctx, "UserService().GetByUsername", http.StatusBadRequest, err.Error())
			return
		}

		if err = helper.ComparePasswords([]byte(user.Password), []byte(req.Password)); err != nil {
			h.log.Error("username or password incorrect")
			h.handlerResponse(ctx, "username or password incorrect", http.StatusBadRequest, "username or password incorrect")
			return
		}

		m := make(map[string]interface{})
		m["user_id"] = user.Id
		m["branch_id"] = user.BranchId
		m["username"] = user.Login

		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
		if err != nil {
			h.log.Error("error while generate jwt token", logger.Error(err))
			h.handlerResponse(ctx, "error while generate jwt token", http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusCreated, SignInResp{Token: token})
	} else {
		courier, err := h.services.CourierService().GetByUsername(ctx, &user_service.CourierGetByUsernameReq{
			Username: req.Username,
		})
		if err != nil {
			h.log.Error("error while getting courier by username:", logger.Error(err))
			h.handlerResponse(ctx, "CourierService().GetByUsername", http.StatusBadRequest, err.Error())
			return
		}

		if err = helper.ComparePasswords([]byte(courier.Password), []byte(req.Password)); err != nil {
			h.log.Error("username or password incorrect")
			h.handlerResponse(ctx, "username or password incorrect", http.StatusBadRequest, err.Error())
			return
		}

		m := make(map[string]interface{})
		m["courier_id"] = courier.Id
		m["branch_id"] = courier.BranchId
		m["username"] = courier.Login

		token, err := helper.GenerateJWT(m, config.TokenExpireTime, config.JWTSecretKey)
		if err != nil {
			h.log.Error("error while generate jwt token", logger.Error(err))
			h.handlerResponse(ctx, "error while generate jwt token", http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusCreated, SignInResp{Token: token})
	}
}

type ChangePasswordReq struct {
	Email string `json:"email"`
	Role  string `json:"role"`
}

func (h *Handler) ChangePassword(ctx *gin.Context) {
	var req ChangePasswordReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handlerResponse(ctx, "ShouldBindJSON()", http.StatusBadRequest, err.Error())
		return
	}

	if req.Role == "user" {
		res, err := helper.SendMail(req.Email, fmt.Sprintf("Your verification code: "+helper.GenerateCode()))
		if err != nil {
			h.log.Error("error while send email", logger.Error(err))
			h.handlerResponse(ctx, "SendMail()", http.StatusBadRequest, err.Error())
			return
		}

		ctx.JSON(http.StatusOK, res)
	}
}
