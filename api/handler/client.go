package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/user_service"
)

// CreateClient godoc
// @Router       /v1/clients [post]
// @Summary      Create a new client
// @Description  Create a new client with the provided details
// @Tags         Clients
// @Accept       json
// @Produce      json
// @Param        Clients     body  user_service.CreateClientReq  true  "data of the client"
// @Success      201  {object}  user_service.CreateClientResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateClient(ctx *gin.Context) {
	var client = user_service.Client{}

	err := ctx.ShouldBindJSON(&client)
	if err != nil {
		h.handlerResponse(ctx, "CreateClient", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ClientService().Create(ctx, &user_service.ClientCreateReq{
		FirstName:      client.FirstName,
		LastName:       client.LastName,
		Phone:          client.Phone,
		Photo:          client.Photo,
		BirthDate:      client.BirthDate,
		DiscountType:   client.DiscountType,
		DiscountAmount: client.DiscountAmount,
	})

	if err != nil {
		h.handlerResponse(ctx, "ClientService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create client response", http.StatusOK, resp)
}

// ListClients godoc
// @Router       /v1/clients [get]
// @Summary      List clients
// @Description  Get clients
// @Tags         Clients
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   user_service.Client
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListClient(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("offset", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ClientService().GetList(ctx.Request.Context(), &user_service.ClientGetListReq{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListClient", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list client response", http.StatusOK, resp)
}

// GetClient godoc
// @Router       /v1/clients/{id} [get]
// @Summary      Get a client by ID
// @Description  Retrieve a client by its unique identifier
// @Tags         Clients
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "User ID to retrieve"
// @Success      200  {object}  user_service.Client
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetClient(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.ClientService().GetById(ctx.Request.Context(), &user_service.ClientIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error client GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get client response", http.StatusOK, resp)
}

// UpdateClient godoc
// @Router       /v1/clients/{id} [put]
// @Summary      Update an existing client
// @Description  Update an existing client with the provided details
// @Tags         Clients
// @Accept       json
// @Produce      json
// @Param        id       path    string     true    "Client ID to update"
// @Param        client   body    user_service.ClientUpdateReq  true    "Updated data for the client"
// @Success      200  {object}  user_service.ClientUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateClient(ctx *gin.Context) {
	var client = user_service.Client{}
	client.Id = ctx.Param("id")
	err := ctx.ShouldBindJSON(&client)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ClientService().Update(ctx.Request.Context(), &user_service.ClientUpdateReq{
		Id:             client.Id,
		FirstName:      client.FirstName,
		LastName:       client.LastName,
		Phone:          client.Phone,
		Photo:          client.Photo,
		BirthDate:      client.BirthDate,
		DiscountType:   client.DiscountType,
		DiscountAmount: client.DiscountAmount,
	})

	if err != nil {
		h.handlerResponse(ctx, "error client Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update client response", http.StatusOK, resp.Msg)
}

// DeleteClient godoc
// @Router       /v1/clients/{id} [delete]
// @Summary      Delete a client
// @Description  Delete a client by its unique identifier
// @Tags         Clients
// @Accept       json
// @Produce      json
// @Param        id   path    string     true    "Client ID to retrieve"
// @Success      200  {object}  user_service.ClientDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteClient(ctx *gin.Context) {
	id := ctx.Param("id")

	resp, err := h.services.ClientService().Delete(ctx.Request.Context(), &user_service.ClientIdReq{Id: id})
	if err != nil {
		h.handlerResponse(ctx, "error client Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete client response", http.StatusOK, resp.Msg)
}
