package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/order_service"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/pkg/logger"
)

// CreateOrder godoc
// @Router       /v1/orders [post]
// @Summary      Create a new order
// @Description  Create a new order with the provided details
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        order     body  order_service.OrderCreateReq  true  "data of the order"
// @Success      201  {object}  order_service.OrderCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateOrder(ctx *gin.Context) {
	var order = order_service.OrderCreateReq{}

	err := ctx.ShouldBindJSON(&order)
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Create(ctx, &order_service.OrderCreateReq{
		ClientId:      order.ClientId,
		BranchId:      order.BranchId,
		Type:          order.Type,
		Address:       order.Address,
		CourierId:     order.CourierId,
		Price:         order.Price,
		DeliveryPrice: order.DeliveryPrice,
		Discount:      order.Discount,
		Status:        order.Status,
		PaymentType:   order.PaymentType,
	})

	if err != nil {
		h.handlerResponse(ctx, "UserService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create user response", http.StatusOK, resp)

}

// ListOrders godoc
// @Router       /v1/orders [get]
// @Summary      List orders
// @Description  get orders
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   order_service.Order
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListOrder(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetList(ctx.Request.Context(), &order_service.OrderGetListReq{
		Page:        int64(page),
		Limit:       int64(limit),
		OrderId:     ctx.Query("order_id"),
		ClientId:    ctx.Query("client_id"),
		BranchId:    ctx.Query("branch_id"),
		Typ:         ctx.Query("type"),
		CourierId:   ctx.Query("courier_id"),
		PriceFrom:   ctx.Query("price_from"),
		PriceTo:     ctx.Query("price_to"),
		PaymentType: ctx.Query("payment_type"),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListOrder", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list order response", http.StatusOK, resp)
}

// GetOrder godoc
// @Router       /v1/orders/{id} [get]
// @Summary      Get a order by ID
// @Description  Retrieve a order by its unique identifier
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "Order ID to retrieve"
// @Success      200  {object}  order_service.Order
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetOrder(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetById(ctx.Request.Context(), &order_service.OrderIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error order GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get order response", http.StatusOK, resp)
}

// UpdateOrder godoc
// @Router       /v1/orders/{id} [put]
// @Summary      Update an existing order
// @Description  Update an existing order with the provided details
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "User ID to update"
// @Param        order   body    order_service.OrderUpdateReq  true    "Updated data for the order"
// @Success      200  {object}  order_service.OrderUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateOrder(ctx *gin.Context) {
	var order = order_service.Order{}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}
	order.Id = int64(id)

	err = ctx.ShouldBindJSON(&order)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Update(ctx.Request.Context(), &order_service.OrderUpdateReq{
		Id:            order.Id,
		ClientId:      order.ClientId,
		BranchId:      order.BranchId,
		Type:          order.Type,
		Address:       order.Address,
		CourierId:     order.CourierId,
		Price:         order.Price,
		DeliveryPrice: order.DeliveryPrice,
		Discount:      order.Discount,
		Status:        order.Status,
		PaymentType:   order.PaymentType,
	})

	if err != nil {
		h.handlerResponse(ctx, "error user Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update user response", http.StatusOK, resp.Msg)
}

// DeleteOrder godoc
// @Router       /v1/orders/{id} [delete]
// @Summary      Delete a order
// @Description  delete a order by its unique identifier
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "Order ID to retrieve"
// @Success      200  {object}  order_service.OrderDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteOrder(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Delete(ctx.Request.Context(), &order_service.OrderIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error order Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete order response", http.StatusOK, resp.Msg)
}

type ChangeOrderStatusReq struct {
	Status string `json:"status"`
}

// ChangeStatusOrder godoc
// @Router       /v1/orders/change-status/{id} [put]
// @Summary      Update an existing order
// @Description  Update an existing order with the provided details
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "Order ID to update"
// @Param        status   body    ChangeOrderStatusReq true    "Updated data for the order"
// @Success      200  {object}  order_service.OrderChangeStatusResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) ChangeOrderStatus(ctx *gin.Context) {
	var req ChangeOrderStatusReq
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		ctx.JSON(http.StatusBadRequest, "invalid body")
		return
	}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().ChangeStatus(ctx.Request.Context(), &order_service.OrderChangeStatusReq{
		Id:     int64(id),
		Status: req.Status,
	})

	if err != nil {
		h.handlerResponse(ctx, "OrderService().ChangeStatus", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "response order change status", http.StatusOK, resp.Msg)
}
