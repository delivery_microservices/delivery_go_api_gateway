package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/user_service"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/pkg/helper"
)

// CreateUser godoc
// @Router       /v1/users [post]
// @Summary      Create a new user
// @Description  Create a new user with the provided details
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        user     body  user_service.UserCreateReq  true  "data of the product"
// @Success      201  {object}  user_service.UserCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateUser(ctx *gin.Context) {
	var user = user_service.UserCreateReq{}

	err := ctx.ShouldBindJSON(&user)
	if err != nil {
		h.handlerResponse(ctx, "CreateUser", http.StatusBadRequest, err.Error())
		return
	}

	hashPass, err := helper.GeneratePasswordHash(user.Password)
	if err != nil {
		h.handlerResponse(ctx, "GeneratePasswordHash", http.StatusInternalServerError, err.Error())
		return
	}

	resp, err := h.services.UserService().Create(ctx, &user_service.UserCreateReq{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		BranchId:  user.BranchId,
		Phone:     user.Phone,
		Login:     user.Login,
		Password:  string(hashPass),
	})

	if err != nil {
		h.handlerResponse(ctx, "UserService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create user response", http.StatusOK, resp)
}

// ListUsers godoc
// @Router       /v1/users [get]
// @Summary      List users
// @Description  get users
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   user_service.User
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListUser(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	// active := ctx.DefaultQuery("active", "true")
	resp, err := h.services.UserService().GetList(ctx.Request.Context(), &user_service.UserGetListReq{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListUser", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list user response", http.StatusOK, resp)
}

// GetUser godoc
// @Router       /v1/users/{id} [get]
// @Summary      Get a user by ID
// @Description  Retrieve a user by its unique identifier
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "User ID to retrieve"
// @Success      200  {object}  user_service.User
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetUser(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().GetById(ctx.Request.Context(), &user_service.UserIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error user GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get user response", http.StatusOK, resp)
}

// UpdateUser godoc
// @Router       /v1/users/{id} [put]
// @Summary      Update an existing user
// @Description  Update an existing user with the provided details
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "User ID to update"
// @Param        product   body    user_service.UserUpdateReq  true    "Updated data for the user"
// @Success      200  {object}  user_service.UserUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateUser(ctx *gin.Context) {
	var user = user_service.User{}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}
	user.Id = int64(id)

	err = ctx.ShouldBindJSON(&user)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().Update(ctx.Request.Context(), &user_service.UserUpdateReq{
		Id:        user.Id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		BranchId:  user.BranchId,
		Phone:     user.Phone,
		Active:    user.Active,
		Login:     user.Login,
		Password:  user.Password,
	})

	if err != nil {
		h.handlerResponse(ctx, "error user Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update user response", http.StatusOK, resp.Msg)
}

// DeleteUser godoc
// @Router       /v1/users/{id} [delete]
// @Summary      Delete a user
// @Description  delete a user by its unique identifier
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "User ID to retrieve"
// @Success      200  {object}  user_service.UserDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteUser(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().Delete(ctx.Request.Context(), &user_service.UserIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error user Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete user response", http.StatusOK, resp.Msg)
}
