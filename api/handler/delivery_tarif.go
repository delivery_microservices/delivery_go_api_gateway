package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/order_service"
)

// CreateDeliveryTarif godoc
// @Router       /v1/delivery_tarif [post]
// @Summary      Create a new delivery_tarif
// @Description  Create a new delivery_tarif with the provided details
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        delivery_tarif     body  order_service.TarifCreateReq  true  "data of the delivery_tarif"
// @Success      201  {object}  order_service.TarifCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateDeliveryTarif(ctx *gin.Context) {
	var delivery_tarif = order_service.TarifCreateReq{}

	err := ctx.ShouldBindJSON(&delivery_tarif)
	if err != nil {
		h.handlerResponse(ctx, "CreateTarif", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.DeliveryTarifService().Create(ctx, &order_service.TarifCreateReq{
		Name:      delivery_tarif.Name,
		Typ:       delivery_tarif.Typ,
		BasePrice: delivery_tarif.BasePrice,
	})

	if err != nil {
		h.handlerResponse(ctx, "DeliveryTarifService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create deliveryTarif response", http.StatusOK, resp)
}

// ListDeliveryTarif godoc
// @Router       /v1/delivery_tarif [get]
// @Summary      List delivery_tarif
// @Description  get delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   order_service.DeliveryTarif
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListTarif(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	// active := ctx.DefaultQuery("active", "true")
	resp, err := h.services.DeliveryTarifService().GetList(ctx.Request.Context(), &order_service.TarifGetListReq{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListTarif", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get tarif user response", http.StatusOK, resp)
}

// GetDeliveryTarif godoc
// @Router       /v1/delivery_tarif/{id} [get]
// @Summary      Get a delivery_tarif by ID
// @Description  Retrieve a delivers_tarif by its unique identifier
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "Delivery_Tarif ID to retrieve"
// @Success      200  {object}  order_service.DeliveryTarif
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetDeliveryTarif(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.DeliveryTarifService().GetById(ctx.Request.Context(), &order_service.TarifIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error delivery_tarif GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get delivery_tarif response", http.StatusOK, resp)
}

// UpdateTarif godoc
// @Router       /v1/delivery_tarif/{id} [put]
// @Summary      Update an existing delivery_tarif
// @Description  Update an existing delivery_tarif with the provided details
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "Delivery_tarif ID to update"
// @Param        delivery_tarif   body    order_service.DeliverTarifUpdateReq  true    "Updated data for the delivery_tarif"
// @Success      200  {object}  order_service.TarifUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateTarif(ctx *gin.Context) {
	var delivery_tarif = order_service.DeliveryTarif{}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}
	delivery_tarif.Id = int64(id)

	err = ctx.ShouldBindJSON(&delivery_tarif)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.DeliveryTarifService().Update(ctx.Request.Context(), &order_service.TarifUpdateReq{
		Id:        delivery_tarif.Id,
		Name:      delivery_tarif.Name,
		Typ:       delivery_tarif.Typ,
		BasePrice: delivery_tarif.BasePrice,
	})

	if err != nil {
		h.handlerResponse(ctx, "error delivery_tarif Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update delivery_tarif response", http.StatusOK, resp.Msg)
}

// DeletedTarif godoc
// @Router       /v1/delivery_tarif/{id} [delete]
// @Summary      Delete a delivery_tarif
// @Description  delete a delivery_tarif by its unique identifier
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "User ID to retrieve"
// @Success      200  {object}  order_service.TarifDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteTarif(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.DeliveryTarifService().Delete(ctx.Request.Context(), &order_service.TarifIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error delivery_tarif Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete delivery_tarif response", http.StatusOK, resp.Msg)
}
