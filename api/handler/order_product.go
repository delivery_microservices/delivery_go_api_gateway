package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery_microservices/delivery_go_api_gateway/genproto/order_service"
)

// CreateOrderProduct godoc
// @Router       /v1/order_products [post]
// @Summary      Create a new order_product
// @Description  Create a new order_product with the provided details
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        order_products     body  order_product_service.OrderProductCreateReq  true  "data of the order"
// @Success      201  {object}  order_product_service.OrderProductCreateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateOrderProduct(ctx *gin.Context) {
	var order_product = order_service.OrderProductCreateReq{}

	err := ctx.ShouldBindJSON(&order_product)
	if err != nil {
		h.handlerResponse(ctx, "CreateOrder", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderProductService().Create(ctx, &order_service.OrderProductCreateReq{
		OrderId:   order_product.OrderId,
		ProductId: order_product.ProductId,
		Quantity:  order_product.Quantity,
		Price:     order_product.Price,
	})

	if err != nil {
		h.handlerResponse(ctx, "UserService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create user response", http.StatusOK, resp)

}

// ListOrderProducts godoc
// @Router       /v1/order_products [get]
// @Summary      List order_products
// @Description  get order_products
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        limit    query     int  false  "limit for response"  Default(10)
// @Param		 page     query     int  false  "page for response"   Default(1)
// @Success      200  {array}   order_service.OrderProduct
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetListOrderProduct(ctx *gin.Context) {
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		h.handlerResponse(ctx, "error get page", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		h.handlerResponse(ctx, "error get limit", http.StatusBadRequest, err.Error())
		return
	}

	// active := ctx.DefaultQuery("active", "true")
	resp, err := h.services.OrderProductService().GetList(ctx.Request.Context(), &order_service.OrderProductGetListReq{
		Page:  int64(page),
		Limit: int64(limit),
	})

	if err != nil {
		h.handlerResponse(ctx, "error GetListOrderProduct", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list order_product response", http.StatusOK, resp)
}

// GetOrderProduct godoc
// @Router       /v1/order_products/{id} [get]
// @Summary      Get a order_product by ID
// @Description  Retrieve a order_product by its unique identifier
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "OrderProduct ID to retrieve"
// @Success      200  {object}  order_service.OrderProduct
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) GetOrderProduct(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderProductService().GetById(ctx.Request.Context(), &order_service.OrderProductIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error order_product GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get order_product response", http.StatusOK, resp)
}

// UpdateOrderProduct godoc
// @Router       /v1/order_products/{id} [put]
// @Summary      Update an existing order_products
// @Description  Update an existing order_products with the provided details
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        id       path    int     true    "OrderProduct ID to update"
// @Param        order   body    order_service.OrderProductUpdateReq  true    "Updated data for the order_product"
// @Success      200  {object}  order_service.OrderProductUpdateResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) UpdateOrderProduct(ctx *gin.Context) {
	var order_product = order_service.OrderProduct{}
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}
	order_product.Id = int64(id)

	err = ctx.ShouldBindJSON(&order_product)
	if err != nil {
		h.handlerResponse(ctx, "error while binding", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderProductService().Update(ctx.Request.Context(), &order_service.OrderProductUpdateReq{
		OrderId:   order_product.OrderId,
		ProductId: order_product.ProductId,
		Quantity:  order_product.Quantity,
	})

	if err != nil {
		h.handlerResponse(ctx, "error order_product Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "update order_product response", http.StatusOK, resp.Msg)
}

// DeleteOrderProduct godoc
// @Router       /v1/order_products/{id} [delete]
// @Summary      Delete a order_products
// @Description  delete a order_products by its unique identifier
// @Tags         order_products
// @Accept       json
// @Produce      json
// @Param        id   path    int     true    "OrderProduct ID to retrieve"
// @Success      200  {object}  order_service.OrderProductDeleteResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) DeleteOrderProduct(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		h.handlerResponse(ctx, "bad request", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderProductService().Delete(ctx.Request.Context(), &order_service.OrderProductIdReq{Id: int64(id)})
	if err != nil {
		h.handlerResponse(ctx, "error order product Delete", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "delete order product response", http.StatusOK, resp.Msg)
}
